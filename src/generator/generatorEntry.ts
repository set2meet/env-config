#!/usr/bin/env node
/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import 'colors';
import fs from 'fs-extra';
import { compareConfigs, execScripts, insertSecrets } from './utils/utils';
import 'polyfill-object.fromentries';
import getSecrets from './vault/vault';
import path from 'path';
import { secretPaths } from '../common/constants';

const CONFIG_LOCATION = 'generator';
let envConfigs = {};
let NAMESPACE = '';
let HASHICORP_URL = '';

const setEnvConfigs = (NAMESPACE) => {
  envConfigs = {
    development: {
      namespace: NAMESPACE || 'bss-dev/epm-s2m',
      urlPrefix: 'dev/',
    },
    qa: {
      namespace: NAMESPACE || 'bss-dev/epm-s2m',
      urlPrefix: 'qa/',
    },
    local: {
      namespace: NAMESPACE || 'bss-dev/epm-s2m',
      urlPrefix: 'local/',
    },
    'new-production': {
      namespace: NAMESPACE || 'bss/epm-s2m',
      urlPrefix: '',
    },
    production: {
      namespace: NAMESPACE || 'bss/epm-s2m',
      urlPrefix: '',
    },
  };
};

// validate arguments
const validateArguments = (args) =>
  new Promise((resolve, reject) => {
    const errors = [];
    const optForceKey = '-force';
    const optRoleKey = '-role_id';
    const optSecretKey = '-secret_id';
    const optStandKey = '-stand';
    const optModuleKey = '-module';
    const optPathKey = '-path';
    const optURLKey = '-url';
    const optNamespaceKey = '-namespace';
    const optionForceIdInx = args.indexOf(optForceKey);
    const optionRoleIdInx = args.indexOf(optRoleKey);
    const optionSecretIdInx = args.indexOf(optSecretKey);
    const optionStandNameInx = args.indexOf(optStandKey);
    const optionModuleNameInx = args.indexOf(optModuleKey);
    const optionPathNameInx = args.indexOf(optPathKey);
    const optionURLInx = args.indexOf(optURLKey);
    const optionNamespaceInx = args.indexOf(optNamespaceKey);
    const errorNoOptionKey = (key) => errors.push(`Error: required '${key} option in arguments`);
    const createOptionExample = (key, value) => errors.push(` > example "${key} ${value}"`);
    const errorNoOptionValue = (key) => errors.push(`Error: option '${key} undefined`);

    if (optionRoleIdInx === -1 || optionSecretIdInx === -1) {
      const message = 'There are no keys from CI. Dev stand will be used';
      console.warn(message);
    }

    if (optionStandNameInx === -1) {
      errorNoOptionKey(optStandKey);
      createOptionExample(optStandKey, 'production');
    }
    if (optionModuleNameInx === -1) {
      errorNoOptionKey(optModuleKey);
      createOptionExample(optModuleKey, 'recording-capture');
    }
    if (optionPathNameInx === -1) {
      errorNoOptionKey(optPathKey);
      createOptionExample(optPathKey, '.\\config\\default.json');
    }

    const optionRoleId = optionRoleIdInx === -1 ? undefined : args[optionRoleIdInx + 1];
    const optionSecretId = optionSecretIdInx === -1 ? undefined : args[optionSecretIdInx + 1];
    const optionStandName = args[optionStandNameInx + 1];
    const optionModuleName = args[optionModuleNameInx + 1];
    const optionPathName = args[optionPathNameInx + 1];

    HASHICORP_URL = optionURLInx === -1 ? undefined : args[optionURLInx + 1];
    NAMESPACE = optionNamespaceInx === -1 ? undefined : args[optionNamespaceInx + 1];

    setEnvConfigs(NAMESPACE);

    if (!optionStandName) {
      errorNoOptionValue(optStandKey);
    }
    if (!optionModuleName) {
      errorNoOptionValue(optModuleKey);
    }
    if (!optionPathName) {
      errorNoOptionValue(optionPathName);
    }

    if (errors.length === 0) {
      resolve({
        standName: optionStandName,
        moduleName: optionModuleName,
        pathName: optionPathName,
        roleId: optionRoleId,
        secretId: optionSecretId,
        force: optionForceIdInx !== -1,
      });
    } else {
      reject(errors);
    }
  });

// import all existed stand configurations in all available modules
const readDirItems = (dir, method) => {
  return Promise.resolve()
    .then(() => fs.readdir(dir))
    .then((items) => items.map((item) => path.resolve(dir, item)))
    .then((items) => Promise.all(items.map((item) => fs.stat(item).then((stats) => (stats[method]() ? item : null)))))
    .then((items) => items.filter((item) => item !== null))
    .catch((err) => {
      throw [`Error: can not read directory '${dir}'`, ` > ${err.message}`];
    });
};

const importConfigsPerModule = (options) =>
  new Promise(async (resolve, reject) => {
    const errors = [];
    const baseDir = path.resolve(__dirname, CONFIG_LOCATION);

    // collect all modules by its names into the array
    const modules = await readDirItems(baseDir, 'isDirectory');

    // read all modules and collect their configs into the hash map object
    const configs = (
      await Promise.all(
        modules.map(async (modulePath: string) => {
          const moduleConfigs = await readDirItems(modulePath, 'isFile');
          const standsConfig = moduleConfigs.map((src: string) => ({
            src,
            name: path.basename(src).split('.')[0],
          }));

          return {
            standsConfig,
            moduleName: path.basename(modulePath),
          };
        })
      )
    )
      // build configs model as Record<moduleName, Record<standName, standConfig>>
      .reduce((lib, md) => {
        lib[md.moduleName] = md.standsConfig.reduce((stands, cfg) => {
          stands[cfg.name] = require(cfg.src);
          return stands;
        }, {});
        return lib;
      }, {});

    // check existences of config by options parameters
    if (!configs[options.moduleName]) {
      errors.push(`Error: not found module '${options.moduleName}' in the sources`);
      return reject(errors);
    }
    if (!configs[options.moduleName][options.standName]) {
      errors.push(
        `Error: not found stand config '${options.standName}' in the sources of the module '${options.moduleName}'`
      );
      return reject(errors);
    }

    resolve({
      config: configs[options.moduleName][options.standName],
      filePath: options.pathName,
      standName: options.standName,
      roleId: options.roleId,
      secretId: options.secretId,
      force: options.force,
    });
  });

const buildConfigAsDefaultJSON = async (configObj) =>
  new Promise((resolve, reject) => {
    const { config, filePath, secrets } = configObj;
    const outputData = JSON.stringify(config, null, 2);
    const outputDir = path.dirname(filePath);

    Promise.resolve()
      .then(() => fs.remove(outputDir))
      .then(() => fs.ensureDir(outputDir))
      .then(() => fs.writeFile(filePath, outputData))
      .then(() => resolve({ config, secrets }))
      .catch((err) => reject(['Error: can not build config file:', ` > ${err.message}`]));
  });

const stopProcessAndShowErrors = (errors) => {
  console.log(errors.message || errors.join('\n'));
  console.log('\n');
  process.exit(1);
};

const validateLocalConfig = (localConfig, generatedConfig) => {
  const identical =
    'Local and Generated config parts are identical. If you still want to generate config please use -force option';
  const notIdentical =
    "Local and Generated config parts are NOT identical. Probably Local part has been updated. It'd be better to update your config through -force option";

  if (compareConfigs(localConfig, generatedConfig)) {
    console.info(identical.green);
  } else {
    console.warn(`[WARN]`.white.bgYellow);
    console.warn(`${notIdentical}`.yellow);
  }
};

const getUrls = (envName) => {
  const prefix = envConfigs[envName].urlPrefix;
  return secretPaths.map((url) => `${prefix}${url}`);
};

const removeEnvPrefixFromSecrets = (prefix, secrets) => {
  return Object.keys(secrets).reduce((obj, key) => {
    const newKey = key.substr(prefix.length);

    obj[newKey] = secrets[key];

    return obj;
  }, {});
};

export const buildJSONConfig = async () =>
  Promise.resolve(process.argv)
    .then(validateArguments)
    .then(importConfigsPerModule)
    .then(async (config: any) => {
      const envConfig = envConfigs[config.standName];
      const secrets = await getSecrets({
        roleId: config.roleId,
        secretId: config.secretId,
        namespace: envConfig.namespace,
        urls: getUrls(config.standName),
        hashicorpURL: HASHICORP_URL,
      });

      const clearedSecrets = removeEnvPrefixFromSecrets(envConfig.urlPrefix, secrets);
      const localConfig = fs.existsSync(config.filePath) ? fs.readJsonSync(config.filePath) : '';

      //TODO Remove this part when Vault would be setup with VPN
      if (config.roleId && config.secretId) {
        insertSecrets(config.config, clearedSecrets);
        buildConfigAsDefaultJSON({
          ...config,
          secrets: clearedSecrets,
        });
        return;
      }

      //If we have static file (localConfig), don't have 'force' option we can exec scripts here
      if (!config.force && localConfig !== '') {
        validateLocalConfig(localConfig, config.config);
        execScripts({
          config: localConfig,
          secrets: clearedSecrets,
        });
        return;
      }

      buildConfigAsDefaultJSON({
        ...config,
        secrets: clearedSecrets,
      }).then(execScripts);
    })
    .catch(stopProcessAndShowErrors);

buildJSONConfig();
