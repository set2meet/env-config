/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import readlineSync from 'readline-sync';
import flatCache from 'flat-cache';
import fetch from 'node-fetch';

const cache = flatCache.load('cache-token');

const TOKEN_KEY = 'token';

let VAULT_URL = `https://vault.service.consul.epm-sec.projects.epam.com:8200`;

/**
 * Check env and token.
 * For Dev - If token is null then request new token and request all secrets
 * For Prod - request new short-time token with role_id, secret_id from CI/CD pipeline
 */
async function getSecrets({ roleId, secretId, namespace, urls, hashicorpURL }) {
  const isCIFlow = typeof roleId !== 'undefined' && typeof secretId !== 'undefined';
  let clientToken = cache.getKey(TOKEN_KEY);
  VAULT_URL = hashicorpURL || VAULT_URL;

  /**
   * We have to use this options because of current Vault settings because of certificate's problem
   * @type {string}
   */
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

  if (!isCIFlow) {
    const isTokenFresh = await checkToken(clientToken);
    if (!isTokenFresh) {
      const username = readlineSync.question('Enter your domain username (name_surname): ');
      const password = readlineSync.question('Enter your domain password: ', {
        hideEchoBack: true,
      });
      clientToken = await requestTokenLDAP(username, password);
      cache.setKey(TOKEN_KEY, clientToken);
      cache.save();
    }
  } else {
    clientToken = await requestTokenCI(roleId, secretId, namespace);
  }

  const secrets = await getAllSecrets(clientToken, urls, namespace);

  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '1';

  return secrets;
}

/**
 * Check local token
 * @returns {*|ArrayBuffer|IDBRequest<IDBValidKey | undefined>|string|null}
 */
async function checkToken(token) {
  if (typeof token !== 'undefined') {
    return await verifyToken(token);
  }
  return false;
}

/**
 * Verify existing token
 * @param token
 * @returns {Promise<boolean>}
 */
async function verifyToken(token) {
  const url = `${VAULT_URL}/v1/auth/token/lookup-self`;

  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'X-Vault-Namespace': 'bss-dev/epm-s2m',
      'X-Vault-Token': token,
    },
  });
  const lookup = await response.json();

  return !!lookup.data;
}

/**
 * Request token for local development. Don't forget about DUO
 * @param username
 * @param password
 * @returns {Promise<*>}
 */
async function requestTokenLDAP(username, password) {
  const url = `${VAULT_URL}/v1/auth/ldap/login/${username}`;
  console.log('=================== Please check your DUO app ===================');

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'X-Vault-Namespace': 'bss-dev/epm-s2m',
    },
    body: JSON.stringify({
      password,
    }),
  });
  const token = await response.json();

  if (typeof token.errors !== 'undefined') {
    throw new Error(`Error has appeared in request token operation ->>>>> ${token.errors}`);
  }

  return token.auth.client_token;
}

/**
 * Request token with role_id and secret_id from CI/CD pipeline
 * @param roleId
 * @param secretId
 * @param namespace
 * @returns {Promise<*>}
 */
async function requestTokenCI(roleId, secretId, namespace) {
  const url = `${VAULT_URL}/v1/auth/approle/login`;
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'X-Vault-Namespace': namespace,
    },
    body: JSON.stringify({
      role_id: roleId,
      secret_id: secretId,
    }),
  });
  const token = await response.json();

  if (typeof token.errors !== 'undefined') {
    throw new Error(`Error has appeared in request token operation ->>>>> ${token.errors}`);
  }

  return token.auth.client_token;
}

/**
 * Request all secrets from Vault
 * @param token
 * @param namespace
 * @returns {Promise<{[p: string]: any}>}
 */
async function getAllSecrets(token, urls, namespace) {
  const secrets: any[] = await Promise.all(urls.map((secretURL) => getSecret(secretURL, token, namespace)));

  return Object.fromEntries(secrets.filter(Boolean));
}

/**
 * Request certain secret from Vault with token
 * @param secretPath
 * @param token
 * @param namespace
 * @returns {Promise<{}>}
 */
async function getSecret(secretPath, token, namespace) {
  const url = `${VAULT_URL}/v1/set2meet/${secretPath}`;
  const result = await fetch(url, {
    method: 'GET',
    headers: {
      'X-Vault-Namespace': namespace,
      'X-Vault-Token': token,
    },
  });

  if (result.status === 404) {
    return null;
  }

  const resJSON = await result.json();
  if (typeof resJSON.errors !== 'undefined') {
    throw new Error(resJSON.errors);
  }
  return [[secretPath], resJSON.data];
}

export default getSecrets;
