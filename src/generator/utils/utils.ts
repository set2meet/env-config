/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import diff from 'recursive-diff';
import { exec } from 'child_process';
import util from 'util';
import 'colors';
import { CONFIG_ENV_KEY, STATIC_CONFIG_ENV_KEY } from '../../common/constants';

/**
 * Set value to a path inside object
 * @param object
 * @param objPath
 * @param value
 * @returns {T}
 */
const setPath = (object, objPath, value) => {
  return objPath.split('.').reduce((o, p, i) => {
    return (o[p] = objPath.split('.').length === ++i ? Object.assign(o[p] ? o[p] : {}, value) : o[p] || {});
  }, object);
};

/**
 * Find the appropriate path and insert secrets into it
 * @param config
 * @param secrets
 * @returns {*}
 */
export const insertSecrets = (config, secrets) => {
  Object.keys(secrets).forEach((urlPath) => {
    const objPath = urlPath.replace(/\//g, '.');
    setPath(config, objPath, secrets[urlPath]);
  });
};

/**
 *
 * @param obj
 */
export const execScripts = (obj = { config: {}, secrets: {} }) => {
  if (process.argv.indexOf('-npmScripts') === -1) return;

  const scriptStr = process.argv[process.argv.indexOf('-npmScripts') + 1];
  let config = obj.config;
  let secrets = obj.secrets;
  let execString = '';
  scriptStr.split(',').forEach((value) => {
    execString += `\"npm run ${value}\" `;
  });

  process.env[STATIC_CONFIG_ENV_KEY] = JSON.stringify(config);
  insertSecrets(config, secrets);
  process.env[CONFIG_ENV_KEY] = JSON.stringify(config);

  exec(
    `concurrently --prefix "[{name}]" --names ${scriptStr} ${execString}`,
    { maxBuffer: 1024 * 1024 * 10 },
    (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`.red);
        return;
      }
      console.log(`stdout: ${stdout}`);

      console.log(`stderr: ${stderr}`.red);
    }
  ).stdout.on('data', function (data) {
    console.log(data);
  });
};

export const compareConfigs = (localConfig, config) => {
  if (JSON.stringify(localConfig) === JSON.stringify(config)) {
    return true;
  } else {
    console.log(`[WARN]`.white.bgYellow);
    console.log(`Differences between local and generated config\r\n`.yellow);
    console.log(
      util.inspect(diff.getDiff(config, localConfig), {
        showHidden: false,
        depth: null,
      }).yellow
    );
    return false;
  }
};
