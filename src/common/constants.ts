/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

export const CONFIG_ENV_KEY = 'NODE_CONFIG';
export const STATIC_CONFIG_ENV_KEY = 'STATIC_CONFIG_PART';
export const secretPaths = [
  'auth/realmlist/form/Set2Meet',
  'auth/realmlist/form/LOCAL_ENV',
  'feedback/smtp/auth',
  'logger/cloud/aws/credentials',
  'reporting/sql',
  's3c/credentials',
  'sqs/credentials',
  'webrtc/tokbox',
  'resourceManagement/realms/EpamSSO',
  'resourceManagement/realms/Set2Meet',
  'notepad',
  'codeEditor',
  'httpsOptions',
];
