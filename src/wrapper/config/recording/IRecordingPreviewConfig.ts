/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { ICommonConfig } from '../ICommonConfig';
import IRecordingConfig from './IRecordingConfig';
import { IResourceManagementWrapper } from '../resourceManagement/IResourceManagementConfig';
import { IRecordingDbConfig } from './IRecordingDbConfig';

export interface IPreviewConfig {
  nextTaskTimeout?: number;
  searchTaskTimeout?: number;
  waitTimeout?: number;
  visibilityTimeout?: number;
}

export interface IRecordingPreviewConfig extends ICommonConfig, IRecordingConfig, IRecordingDbConfig {
  recordingPreview?: IPreviewConfig;
  resourceManagement: IResourceManagementWrapper;
}
