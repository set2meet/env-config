/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */
import { RedisOptions } from 'ioredis';
import { ICommonConfig } from '../ICommonConfig';

export enum WebrtcServiceType {
  OPENTOK = '@OPENTOK',
  P2P = '@PEER_TO_PEER',
}

export interface IAppPresentationsServiceConfig {
  url: string;
  ws: string;
  wss: string;
  path: {
    upload: string;
    download: string;
  };
}
export interface IAuthConfig {
  strategy: string;
  realmlist: any;
  localUrl: string;
  realm: string;
  allowFakeUsers?: boolean;
  allowChangeAuthMethods?: boolean;
}
export interface TokBoxServiceConfig {
  API_KEY: string;
  API_SECRET: string;
  clientBundle: string;
}
export type TSmtpConfig = {
  route: string;
  smtp: {
    host: string;
    port: number;
    secure: boolean;
    auth: { user: string; pass: string };
  };
  mail: {
    params: { from: string; to: string; subject: string };
  };
};

export type TFirebaseConfig = {
  apiKey: string;
  authDomain: string;
  databaseURL: string;
  projectId: string;
  storageBucket: string;
  messagingSenderId: string;
};

export interface IVRConfig extends ICommonConfig {
  notepad: TFirebaseConfig;
  codeEditor: TFirebaseConfig;
  presentations?: {
    service: IAppPresentationsServiceConfig;
  };
  logger?: {
    path: string;
    routes: {
      getLogs: string;
    };
    out: {
      log: string;
    };
  };
  reporting?: {
    sql: {
      host: string;
      database: string;
      username: string;
      password: string;
    };
  };
  auth?: IAuthConfig;
  logLevel?: string;
  redisConfig?: RedisOptions;
  project?: {
    session?: {
      timeout: number;
    };
    port: number;
    name: string;
    urls: {
      home: string;
    };
    routes: {
      userMediaCheck: string;
    };
    contacts: {
      support: string;
    };
    activeTools: string[];
    gui?: {
      disableRecordings?: boolean;
    };
    realmPrefix: string;
  };
  webrtc?: IWebrtcServicesConfig & IWebrtcServicesWrapper;
  feedback?: TSmtpConfig;
}

export interface IWebrtcServicesConfig {
  default: string;
  tokbox?: TokBoxServiceConfig;
  p2p: P2PServiceConfig;

  maxUsers: Record<WebrtcServiceType, number>;
}

export interface P2PServiceConfig {
  RTCConfiguration: RTCConfiguration;
}

export type TWebrtcServiceConfig = P2PServiceConfig | TokBoxServiceConfig;

export interface IWebrtcServicesWrapper {
  getServiceConfigByType(type: WebrtcServiceType): TWebrtcServiceConfig;
  getServiceConfigByName(name: keyof WebrtcServiceType): TWebrtcServiceConfig;
}

export class WebrtcServicesWrapper implements IWebrtcServicesWrapper {
  constructor(private config: IWebrtcServicesConfig) {
    Object.keys(config).forEach((key) => (this[key] = config[key]));
  }

  getServiceConfigByType(type: WebrtcServiceType): TWebrtcServiceConfig {
    return this.getServiceConfigByName(WebrtcServiceType[type]);
  }
  getServiceConfigByName(name: keyof WebrtcServiceType): TWebrtcServiceConfig {
    return this.config[name];
  }
}
