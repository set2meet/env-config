import { GrantType } from './GrantType';

export interface IRealm {
  baseUrl: string;
  grantType: GrantType;
  clientId: string;
  clientSecret: string;
}

export type TRealm = { realmName: string } & IRealm;

export interface IResourceManagementConfig {
  realms: Record<string, IRealm>;
}

export interface IResourceManagementWrapper extends IResourceManagementConfig {
  getRealm: (realmName: string) => TRealm;
}
