import { IRealm, IResourceManagementConfig, IResourceManagementWrapper, TRealm } from './IResourceManagementConfig';

export default class ResourceManagementWrapper implements IResourceManagementWrapper {
  public realms: Record<string, IRealm>;

  constructor(config: IResourceManagementConfig) {
    this.realms = config.realms;
  }

  public getRealm(realmName: string): TRealm {
    return {
      realmName,
      ...this.realms[realmName],
    } as TRealm;
  }
}
