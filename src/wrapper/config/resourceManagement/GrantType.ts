export enum GrantType {
  REFRESH_TOKEN = 'refresh_token',
  PASSWORD = 'password',
  CLIENT_CREDENTIALS = 'client_credentials',
}
