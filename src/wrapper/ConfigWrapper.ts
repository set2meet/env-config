/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import { CONFIG_ENV_KEY, STATIC_CONFIG_ENV_KEY } from '../common/constants';
import * as nodeConfig from 'config';
import ResourceManagementWrapper from './config/resourceManagement/ResourceManagementWrapper';
import { WebrtcServicesWrapper } from './config/virtualRoom/IVRConfig';

export interface IConfigWrapper {
  cleanupSecrets<Config>(): IConfigWrapper & Config;
}

const wrapperMap = {
  resourceManagement: ResourceManagementWrapper,
  webrtc: WebrtcServicesWrapper,
};

export default class ConfigWrapper implements IConfigWrapper {
  constructor(config?: Record<string, unknown>) {
    config = config || this.readConfig();
    // @TODO: validate json config against it's typescript definition
    Object.keys(config).forEach((key) => {
      if (wrapperMap[key]) {
        this[key] = new wrapperMap[key](config[key]);
      } else {
        this[key] = config[key];
      }
    });
  }

  public cleanupSecrets<Config>(): IConfigWrapper & Config {
    // read static config part
    const envConfig = process.env[STATIC_CONFIG_ENV_KEY];

    if (envConfig) {
      return (new ConfigWrapper(JSON.parse(envConfig)) as unknown) as IConfigWrapper & Config;
    } else {
      console.warn('Static config was used because process was called without env-config');
      // fallback to json
      return (new ConfigWrapper(nodeConfig.util.toObject()) as unknown) as IConfigWrapper & Config;
    }
  }

  private readConfig() {
    // read config with secrets
    const envConfig = process.env[CONFIG_ENV_KEY];
    if (envConfig) {
      const parsedConfig = JSON.parse(envConfig);
      const httpsOptions = parsedConfig.httpsOptions;

      if (httpsOptions) {
        // https://github.com/auth0/node-jsonwebtoken/issues/642#issuecomment-585173594
        httpsOptions.cert = (httpsOptions.cert as string).replace(/\\n/gm, '\n');
        httpsOptions.key = (httpsOptions.key as string).replace(/\\n/gm, '\n');
      }

      return parsedConfig;
    } else {
      console.warn('Static config was used because process was called without env-config');
      // fallback to json
      return nodeConfig.util.toObject();
    }
  }
}
