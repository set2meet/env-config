/*
 * Copyright © 2020 EPAM Systems, Inc. All Rights Reserved. All information contained herein is, and remains the
 * property of EPAM Systems, Inc. and/or its suppliers and is protected by international intellectual
 * property law. Dissemination of this information or reproduction of this material is strictly forbidden,
 * unless prior written permission is obtained from EPAM Systems, Inc
 */

import ConfigWrapper, { IConfigWrapper } from './ConfigWrapper';
import { IVRConfig } from './config/virtualRoom/IVRConfig';
import { IPresentationConfig } from './config/IPresentationConfig';
import { IRecordingProcessingConfig } from './config/recording/IRecordingProcessingConfig';
import { IRecordingPreviewConfig } from './config/recording/IRecordingPreviewConfig';
import { IRecordingCaptureConfig } from './config/recording/IRecordingCaptureConfig';

type TConfig =
  | IVRConfig
  | IPresentationConfig
  | IRecordingProcessingConfig
  | IRecordingPreviewConfig
  | IRecordingCaptureConfig;

export function readConfig<Config extends TConfig>(): Promise<IConfigWrapper & Config> {
  return Promise.resolve((new ConfigWrapper() as unknown) as IConfigWrapper & Config);
}

export * from './config/virtualRoom/IVRConfig';
export * from './config/IPresentationConfig';
export * from './config/recording/IRecordingProcessingConfig';
export * from './config/recording/IRecordingPreviewConfig';
export * from './config/recording/IRecordingCaptureConfig';
export * from './config/resourceManagement/IResourceManagementConfig';
export * from './config/resourceManagement/GrantType';
export type { IConfigWrapper };

export { ConfigWrapper };
