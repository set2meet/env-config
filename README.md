## Configurations

Simple tool to store and build configs for modules

### Development

#### Common data

All common configurations, such as AWS system access keys, are stored in the `src/basic.js` file. They can be used in other custom configurations.

#### Custom configs

Configs for each service (module) are stored in the corresponding folder by module name (module name without spaces).

The configuration of the service (module) for each individual stand should be stored in a separate file by the name of the stand.

Configs for local development should be stored in the `local.js` file.

For production and new-production Virtual room's instances it will use CI/CD Token. For others login and password will be requested.

### Caching

In all cases below excluding pure npx module your config will be cached.

If file doesn't exist script will create and cache it.

Cache (is exists) compares only static parts and provides 2 messages. In both cases it doesn't generate a new one for you.

If you still want to update your config please provide `-force` flag.

### Credentials

If you are using LDAP way for config generation your EPAM credentials (without email part) will be requested in following format:

name_surname (example - dmitrii_kasnitskii)
password (example - \***\*\*\*\*\*\***)

If you are getting following error: "ldap operation failed: failed to bind as user". It means that you've made a mistake in your credentials.

### How build configs for a particular module and stand

1. clone project
2. open console in the project directory
3. exec comman `npm i`
4. exec comman `npm run build`
5. exec comman `node ./lib/generator -- -module {module-name} -stand {stand-name} -path {output-config-path} -url {hashicorp-url} -namespace {hashicorp-namespace} -role_id {CI-role-id} -secret_id {CI-secret-id} -npmScripts {script1, script2,...}`
   where:
   - `{module-name}`: name of the service (module) for which we want to build the config, without brackets
   - `{stand-name}`: name of the stand for which we want config will be use, without brackets, or use `local` for local development
   - `{output-config-path}`: path for your config
   - `{CI-role-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
   - `{CI-secret-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
   - `{hashicorp-url}`: For custom hashicorp url.
   - `{hashicorp-namespace}`: For custom hashicorp namespace.
   - `{script1, script2,...}`: Array with _NPM_ scripts that will be executed after merging static and secret parts in memory. Inside code, it transforms into the: `concurrently "npm run script1" "npm run script2"... `
6. if config for pointed stand and module not found - there are will error shown in the console
7. if config was found - there are will be created file `config/default.json`
8. copy created folder `config` to the root of the module directory
9. done

### How to add secrets

1. add secret to voult
2. add secret path to index.js `const getUrls = (envName) => {....`

### How build configs from module

1. clone appropriate module (for example virtual-room)
2. open console in the project directory
3. install all dependencies
4. exec command `npm run start:local` (it depends on a stand that you want)
5. done

### Using as npx module

1. Run in your project the following command `npx "git+ssh://git@gitlab.com:set2meet/env-config.git" build -- -module {module-name} -stand {stand-name} -path ".\config\default.json" -script {scripts[]}`
2. Don't forget about DUO app if you run it not in production environment
3. Check the path from command
4. done
