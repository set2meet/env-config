import dts from 'rollup-plugin-dts';
import esbuild from 'rollup-plugin-esbuild';
import pkg from '../package.json';
import shebang from 'rollup-plugin-preserve-shebang';
import copy from 'rollup-plugin-copy';
import { resolve, normalize } from 'path';

const Format = {
  DTS: 'dts',
  CJS: 'cjs',
  MJS: 'mjs',
  ES: 'es',
};

const inputDir = resolve(__dirname, '../src');
const outputFolder = resolve(__dirname, '../lib');

const configList = ['wrapper'];

const generatorInput = resolve(inputDir, 'generator/generatorEntry.ts');

const external = [...Object.keys(pkg.dependencies || {}), ...Object.keys(pkg.peerDependencies || {})];

const ext = (format) => (format === Format.DTS ? 'd.ts' : format === Format.CJS ? 'js' : Format.MJS);

const bundle = (format, input, output, isBin = false) => {
  const plugins = [];

  if (format === Format.DTS) {
    plugins.push(dts());
  } else {
    plugins.push(
      esbuild({
        tsconfig: 'config/tsconfig.json',
      })
    );
  }

  if (isBin) {
    plugins.push(shebang());
    plugins.push(
      copy({
        targets: [{ src: resolve(inputDir, 'generator/config').replace(/\\/g, '/') + '/*', dest: output }],
      })
    );
  }

  return {
    input,
    output: {
      file: `${output}.${ext(format)}`,
      format: format === Format.CJS ? Format.CJS : Format.CJS.ES,
      sourcemap: format !== Format.DTS,
    },
    plugins,
    external,
  };
};

const result = [bundle(Format.CJS, generatorInput, resolve(outputFolder, 'generator'), true)];

configList.forEach((currentConfig) => {
  const input = resolve(inputDir, `wrapper/${currentConfig}Entry.ts`);
  const output = resolve(outputFolder, currentConfig);

  [Format.CJS, Format.DTS, Format.ES].forEach((format) => {
    result.push(bundle(format, input, output));
  });
});

export default result;
